package dao;

import models.Account;
import models.Agency;
import models.PayingAccount;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PayingAccountDAO implements IDAO<Long, PayingAccount>  {
    private static final String INSERT_QUERY = "INSERT INTO account (balance, agency) VALUES(?,?)";
    private static final String UPDATE_QUERY = "UPDATE account SET balance = ?, agency = ? WHERE id = ?";
    private static final String REMOVE_QUERY = "DELETE FROM account WHERE id = ?";
    private static final String FIND_QUERY = "SELECT * FROM account WHERE id = ?";
    private static final String FIND_ALL_QUERY = "SELECT * FROM account";

    @Override
    public void create(PayingAccount account) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection
                    .prepareStatement( INSERT_QUERY, Statement.RETURN_GENERATED_KEYS ) ) {
                ps.setDouble( 1, account.getBalance() );
                ps.setLong( 2, account.getAgency().getId() );
                ps.executeUpdate();
                try ( ResultSet rs = ps.getGeneratedKeys() ) {
                    if ( rs.next() ) {
                        account.setId( rs.getLong( 1 ) );
                    }
                }
            }
        }
    }

    @Override
    public void update(PayingAccount account) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection.prepareStatement( UPDATE_QUERY ) ) {
                ps.setDouble( 1, account.getBalance() );
                ps.setLong( 2, account.getAgency().getId() );
                ps.setLong( 3, account.getId() );
                ps.executeUpdate();
            }
        }
    }

    @Override
    public void remove(PayingAccount account) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection.prepareStatement( REMOVE_QUERY ) ) {
                account.getAgency().removeAccount(account);
                ps.setLong( 1, account.getId() );
                ps.executeUpdate();
            }
        }
    }

    @Override
    public PayingAccount findById(Long id) throws SQLException, IOException, ClassNotFoundException {
        PayingAccount account = null;
        AgencyDAO agency = new AgencyDAO();
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection.prepareStatement( FIND_QUERY ) ) {
                ps.setLong( 1, id );
                try ( ResultSet rs = ps.executeQuery() ) {
                    if ( rs.next() ) {
                        account = new PayingAccount();
                        account.setId( rs.getLong( "id" ) );
                        account.setBalance( rs.getDouble( "balance" ) );
                        account.setAgency(agency.findById( rs.getLong("agency")) );
                    }
                }
            }
        }
        return account;
    }

    @Override
    public List<PayingAccount> findAll() throws SQLException, IOException, ClassNotFoundException {
        List<PayingAccount> list = new ArrayList<>();
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection.prepareStatement( FIND_ALL_QUERY ) ) {
                try ( ResultSet rs = ps.executeQuery() ) {
                    while ( rs.next() ) {
                        PayingAccount account = new PayingAccount();
                        AgencyDAO agency = new AgencyDAO();
                        account.setId( rs.getLong( "id" ) );
                        account.setBalance( rs.getDouble( "balance" ) );
                        account.setAgency(agency.findById( rs.getLong("agency")) );
                        list.add( account );
                    }
                }
            }
        }
        return list;
    }
}
