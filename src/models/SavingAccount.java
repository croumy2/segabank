package models;

import dao.SavingAccountDAO;

import java.io.IOException;
import java.sql.SQLException;

public class SavingAccount extends Account {

    private float rate;

    public SavingAccount(double balance, Agency agency, float rate) {
        super(balance, agency);
        this.rate = rate;
    }

    public SavingAccount() {}

    @Override
    public void withdrawal(double amount) {
        if (this.balance - amount >= 0){
            this.balance -= amount;
        }
        else {
            System.out.println("Solde insuffisant");
        }
    }

    @Override
    public String toString() {
        if (id == 0){
            SavingAccountDAO dao = new SavingAccountDAO();
            try {
                dao.create(this);
            } catch (SQLException | IOException | ClassNotFoundException e) {
                System.err.println("Impossible de créer le compte en base");
            }
        }
        final StringBuilder sb = new StringBuilder( "Compte Epargne : \n" );
        sb.append( " Compte n°  " ).append( id ).append('\n');
        sb.append( " Solde : " ).append( balance ).append('\n');
        sb.append( " Taux d'intéret : " ).append( rate ).append('\n');
        sb.append( " Prochaine Valeur : " ).append( estimateInterest() ).append('\n');
        sb.append( " Agence : " ).append( agency.getCode() ).append('\n');
        return sb.toString();
    }

    public void interestCalculation(){
        this.balance += estimateInterest();
    }

    public double estimateInterest(){
        return this.balance*this.rate;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }
}
