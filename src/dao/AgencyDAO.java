package dao;

import models.Agency;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AgencyDAO implements IDAO<Long, Agency>{
    private static final String INSERT_QUERY = "INSERT INTO agency (code, address) VALUES(?,?)";
    private static final String UPDATE_QUERY = "UPDATE agency SET code = ?, address = ? WHERE id = ?";
    private static final String REMOVE_QUERY = "DELETE FROM agency WHERE id = ?";
    private static final String FIND_QUERY = "SELECT * FROM agency WHERE id = ?";
    private static final String FIND_ALL_QUERY = "SELECT * FROM agency";

    @Override
    public void create(Agency agency) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection
                    .prepareStatement( INSERT_QUERY, Statement.RETURN_GENERATED_KEYS ) ) {
                ps.setString( 1, agency.getCode() );
                ps.setString( 2, agency.getAddress());
                ps.executeUpdate();
                try ( ResultSet rs = ps.getGeneratedKeys() ) {
                    if ( rs.next() ) {
                        agency.setId( rs.getInt( 1 ) );
                    }
                }
            }
        }
    }

    @Override
    public void update(Agency agency) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection.prepareStatement( UPDATE_QUERY ) ) {
                ps.setString( 1, agency.getCode() );
                ps.setString( 3, agency.getAddress() );
                ps.executeUpdate();
            }
        }
    }

    @Override
    public void remove(Agency agency) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection.prepareStatement( REMOVE_QUERY ) ) {
                ps.setLong( 1, agency.getId() );
                ps.executeUpdate();
            }
        }
    }

    @Override
    public Agency findById(Long id) throws SQLException, IOException, ClassNotFoundException {
        Agency agency = null;
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection.prepareStatement( FIND_QUERY ) ) {
                ps.setLong( 1, id );
                try ( ResultSet rs = ps.executeQuery() ) {
                    if ( rs.next() ) {
                        agency = new Agency();
                        agency.setId( rs.getInt( "id" ) );
                        agency.setCode( rs.getString( "code" ) );
                        agency.setAddress( rs.getString( "address" ) );
                    }
                }
            }
        }
        return agency;
    }

    @Override
    public List<Agency> findAll() throws SQLException, IOException, ClassNotFoundException {
        List<Agency> list = new ArrayList<>();
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection.prepareStatement( FIND_ALL_QUERY ) ) {
                try ( ResultSet rs = ps.executeQuery() ) {
                    while ( rs.next() ) {
                        Agency agency = new Agency();
                        agency.setId( rs.getLong( "id" ) );
                        agency.setCode( rs.getString( "code" ) );
                        agency.setCode( rs.getString( "address" ) );
                        list.add( agency );
                    }
                }
            }
        }
        return list;
    }
}
