import models.*;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class App {

    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        Agency agency = new Agency("NTE44100", "rue de la banque", new ArrayList<Account>());

        Account simple = new SimpleAccount(300, agency, 200);

        Account saving = new SavingAccount(300, agency, (float) 0.5);

        Account paying = new PayingAccount(300, agency);

        agency.printAccounts();
        System.out.println("//////////////////////////////////////////::");
        agency.deleteAccount(paying);

        agency.printAccounts();

        //dspMainMenu();
    }

    public static void dspMainMenu() {

        int response;
        boolean first = true;
        do {
            if ( !first ) {
                System.out.println( "Mauvais choix... merci de recommencer !" );
            }
            System.out.println( "======================================" );
            System.out.println( "============ GESTION COMPTE ==========" );
            System.out.println( "======================================" );
            System.out.println( "1 - Ajouter un compte" );
            System.out.println( "2 - Modifier un compte" );
            System.out.println( "3 - Supprimer un compte" );
            System.out.println( "4 - Lister les comptes" );
            System.out.println( "5 - Sauvegarder les comptes" );
            System.out.println( "6 - Quitter" );
            System.out.print( "Entrez votre choix : " );
            try {
                response = sc.nextInt();
            } catch ( InputMismatchException e ) {
                response = -1;
            } finally {
                sc.nextLine();
            }
            first = false;
        } while ( response < 1 || response > 6 );

        switch ( response ) {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
        }
    }
}
