package models;

import dao.PayingAccountDAO;

import java.io.IOException;
import java.sql.SQLException;

public class PayingAccount extends Account {
    private static final float TAX = (float) 0.05;

    public PayingAccount(double balance, Agency agency) {
        super(balance, agency);
    }

    public PayingAccount(){
        super();
    };

    @Override
    public void payment(double amount) {
        amount = amount - amount*TAX;
        if(amount > 0){
            this.balance += amount;
        }
        else{
            System.out.println("Solde insuffisant");
        }
    }

    @Override
    public void withdrawal(double amount) {
        amount = amount + amount*TAX;
        if (this.balance - amount >= 0){
            this.balance -= amount;
        }
        else {
            System.out.println("Solde insuffisant");
        }
    }

    @Override
    public String toString() {
        if (id == 0){
            PayingAccountDAO dao = new PayingAccountDAO();
            try {
                dao.create(this);
            } catch (SQLException | IOException | ClassNotFoundException e) {
                System.err.println("Impossible de créer le compte en base");
            }
        }
        final StringBuilder sb = new StringBuilder( "Compte Payant : \n" );
        sb.append( " Compte n° " ).append( id ).append('\n');
        sb.append( " Solde : " ).append( balance ).append('\n');
        sb.append( " Agence : " ).append( agency.getCode() ).append('\n');
        return sb.toString();
    }
}
