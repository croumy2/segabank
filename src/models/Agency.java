package models;

import dao.AgencyDAO;
import dao.PayingAccountDAO;
import dao.SavingAccountDAO;
import dao.SimpleAccountDAO;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

public class Agency {
    private long id;
    private String code;
    private String address;
    private List<Account> accounts;

    public Agency(String code, String address, List<Account> accounts) {
        this.code = code;
        this.address = address;
        this.accounts = accounts;
    }

    public Agency() {}

    public void addAccount(Account account) throws SQLException, IOException, ClassNotFoundException {
        if (account!= null){
            AgencyDAO dao = new AgencyDAO();
            try {
                dao.create(this);
            } catch (SQLIntegrityConstraintViolationException e){
                System.out.printf("L'agence %s n'a pas besoin d'être créée %n",account.getAgency().getCode());
            }
            finally {
                this.accounts.add(account);
            }
        }
        else {
            System.out.println("Compte invalide");
        }
    }

    public void deleteAccount(Account account){
            if (account instanceof PayingAccount){
                PayingAccountDAO dao = new PayingAccountDAO();
                try {
                    dao.remove((PayingAccount) account);
                } catch (IOException | SQLException | ClassNotFoundException e) {
                    System.err.println("Impossible de supprimer ce compte");
                }
            } else if (account instanceof SavingAccount){
                SavingAccountDAO dao = new SavingAccountDAO();
                try {
                    dao.remove((SavingAccount) account);
                } catch (IOException | SQLException | ClassNotFoundException e) {
                    System.err.println("Impossible de supprimer ce compte");
                }
            } else if (account instanceof SimpleAccount){
                SimpleAccountDAO dao = new SimpleAccountDAO();
                try {
                    dao.remove((SimpleAccount) account);

                } catch (IOException | SQLException | ClassNotFoundException e) {
                    System.err.println("Impossible de supprimer ce compte");
                }
            }
    }

    public void removeAccount(Account account){
        this.accounts.remove(account);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void printAccounts(){
        for (Account account: accounts) {
            System.out.println(account.toString());
        }
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }
}
