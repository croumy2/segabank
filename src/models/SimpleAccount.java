package models;

import dao.SimpleAccountDAO;

import java.io.IOException;
import java.sql.SQLException;

public class SimpleAccount extends Account {

    private double overdraft;

    public SimpleAccount(double balance, Agency agency, double overdraft) {
        super(balance, agency);
        this.overdraft = overdraft;
    }

    public SimpleAccount() {
    }

    @Override
    public void withdrawal(double amount) {
        if (this.balance + this.overdraft - amount >= 0){
            this.balance -= amount;
        }
        else {
            System.out.println("Solde insuffisant");
        }
    }

    @Override
    public String toString() {
        if (id == 0){
            SimpleAccountDAO dao = new SimpleAccountDAO();
            try {
                dao.create(this);
            } catch (SQLException | IOException | ClassNotFoundException e) {
                System.err.println("Impossible de créer le compte en base");
            }
        }
        final StringBuilder sb = new StringBuilder( "Compte Simple : \n" );
        sb.append( " Compte n° " ).append( id ).append('\n');
        sb.append( " Solde : " ).append( balance ).append('\n');
        sb.append( " Découvert autorisé : " ).append( overdraft ).append('\n');
        sb.append( " Agence : " ).append( agency.getCode() ).append('\n');
        return sb.toString();
    }

    public double getOverdraft() {
        return overdraft;
    }

    public void setOverdraft(double overdraft) {
        this.overdraft = overdraft;
    }
}
