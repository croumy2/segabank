package models;

import java.io.IOException;
import java.sql.SQLException;

public abstract class Account {
    protected long id;
    protected double balance;
    protected Agency agency;


    Account(double balance, Agency agency) {
        this.balance = balance;
        this.agency = agency;
        try {
            agency.addAccount(this);
        } catch (SQLException | IOException | ClassNotFoundException e) {
            e.printStackTrace();
            System.err.printf("Impossible de lier ce compte à l'agence %s",agency.getCode());
        }
    }

    Account() {}


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Agency getAgency() {
        return agency;
    }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    public void payment(double amount){
        if (amount > 0){
            this.balance += amount;
        }
        else {
            System.out.println("Le montant ne peut pas être négatif");
        }
    }
    public abstract void withdrawal(double amount);

    public String toString2(){
        return (this.agency.toString());
    }

    @Override
    public abstract String toString();
}
