package dao;

import models.Agency;
import models.SimpleAccount;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SimpleAccountDAO implements IDAO<Long, SimpleAccount>  {
    private static final String INSERT_QUERY = "INSERT INTO account (balance, agency, overdraft) VALUES(?,?,?)";
    private static final String UPDATE_QUERY = "UPDATE account SET balance = ?, agency = ?, overdraft = ? WHERE id = ?";
    private static final String REMOVE_QUERY = "DELETE FROM account WHERE id = ?";
    private static final String FIND_QUERY = "SELECT * FROM account WHERE id = ?";
    private static final String FIND_ALL_QUERY = "SELECT * FROM account";

    @Override
    public void create(SimpleAccount account) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection
                    .prepareStatement( INSERT_QUERY, Statement.RETURN_GENERATED_KEYS ) ) {
                ps.setDouble( 1, account.getBalance() );
                ps.setLong( 2, account.getAgency().getId() );
                ps.setDouble( 3, account.getOverdraft() );
                ps.executeUpdate();
                try ( ResultSet rs = ps.getGeneratedKeys() ) {
                    if ( rs.next() ) {
                        account.setId( rs.getLong( 1 ) );
                    }
                }
            }
        }
    }

    @Override
    public void update(SimpleAccount account) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection.prepareStatement( UPDATE_QUERY ) ) {
                ps.setDouble( 1, account.getBalance() );
                ps.setLong( 2, account.getAgency().getId() );
                ps.setDouble( 3, account.getOverdraft() );
                ps.setLong( 4, account.getId() );
                ps.executeUpdate();
            }
        }
    }

    @Override
    public void remove(SimpleAccount account) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection.prepareStatement( REMOVE_QUERY ) ) {
                account.getAgency().removeAccount(account);
                ps.setLong( 1, account.getId() );
                ps.executeUpdate();
            }
        }
    }

    @Override
    public SimpleAccount findById(Long id) throws SQLException, IOException, ClassNotFoundException {
        SimpleAccount account = null;
        AgencyDAO agencyDAO = new AgencyDAO();
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection.prepareStatement( FIND_QUERY ) ) {
                ps.setLong( 1, id );
                try ( ResultSet rs = ps.executeQuery() ) {
                    if ( rs.next() ) {
                        account = new SimpleAccount();
                        account.setId( rs.getLong( "id" ) );
                        account.setBalance( rs.getDouble( "balance" ) );
                        account.setAgency(agencyDAO.findById(rs.getLong("agency")) );
                        account.setOverdraft( rs.getFloat("overdraft"));
                    }
                }
            }
        }
        return account;
    }

    @Override
    public List<SimpleAccount> findAll() throws SQLException, IOException, ClassNotFoundException {
        List<SimpleAccount> list = new ArrayList<>();
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection.prepareStatement( FIND_ALL_QUERY ) ) {
                try ( ResultSet rs = ps.executeQuery() ) {
                    while ( rs.next() ) {
                        SimpleAccount account = new SimpleAccount();
                        AgencyDAO agency = new AgencyDAO();
                        account.setId( rs.getLong( "id" ) );
                        account.setBalance( rs.getDouble( "balance" ) );
                        account.setAgency(agency.findById(rs.getLong("agency")) );
                        account.setOverdraft( rs.getFloat("overdraft"));
                        list.add( account );
                    }
                }
            }
        }
        return list;
    }
}
